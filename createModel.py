from keras.models import Sequential, Model
from keras.layers import *
from constants import DISCRIMINATOR_DIM, GENERATOR_DIM
from keras import layers
import tensorflow as tf
import numpy as np


def createDG():
    # Create the discriminator.
    discriminator = Sequential(
        [
            InputLayer(DISCRIMINATOR_DIM),
            Conv2D(24, (3, 3), activation='relu'),
            MaxPooling2D(pool_size=(2, 2)),
            Conv2D(48, (3, 3), activation='relu'),
            MaxPooling2D(pool_size=(2, 2)),
            Conv2D(96, (3, 3), activation='relu'),
            MaxPool2D(pool_size=(2, 2)),
            Flatten(),
            Dense(1024),
            Dense(1)
        ],
        name="discriminator",
    )

    # Create the generator.
    generator = Sequential(
        [
            layers.InputLayer(GENERATOR_DIM),
            Conv2D(48, (3, 3), activation='relu', padding='same'),
            BatchNormalization(),
            Conv2D(24, (3, 3), activation='relu', padding='same'),
            BatchNormalization(),
            Conv2D(12, (3, 3), activation='relu', padding='same'),
            BatchNormalization(),
            Conv2D(12, (3, 3), activation='relu', padding='same'),
            BatchNormalization(),
            Conv2D(3, (3, 3), activation='relu', padding='same')
        ],
        name="generator",
    )
    return generator, discriminator


class Gan(Model):
    def __init__(self, discriminator, generator):
        super(Gan, self).__init__()
        self.discriminator = discriminator
        self.generator = generator

    def compile(self, d_optimizer, g_optimizer):
        super(Gan, self).compile(
            loss=tf.losses.BinaryCrossentropy()
        )
        self.d_optimizer = d_optimizer
        self.g_optimizer = g_optimizer
        self.loss_fn = tf.losses.MeanSquaredError()

    def train_step(self, data):
        X, Y = data

        with tf.GradientTape(persistent=True) as g_tape:
            fake_image = self.generator(X)
            loss_fn_res = self.loss_fn(Y, fake_image)

            with tf.GradientTape(persistent=True) as d_tape:
                disc_fake = self.discriminator(fake_image)
                disc_data = self.discriminator(Y)

                d_tensor = tf.ones(shape=tf.shape(disc_data))
                g_tensor = tf.ones(shape=tf.shape(disc_fake))
                fake_tensor = tf.zeros(shape=tf.shape(disc_fake))

                d_loss = self.compiled_loss(fake_tensor, disc_fake) + self.compiled_loss(d_tensor, disc_data)
                g_loss = self.compiled_loss(g_tensor, disc_fake) + loss_fn_res

        self.g_optimizer.apply_gradients(
            zip(
                g_tape.gradient(g_loss, self.generator.trainable_weights),
                self.generator.trainable_weights
            )
        )

        self.d_optimizer.apply_gradients(
            zip(
                d_tape.gradient(d_loss, self.discriminator.trainable_weights),
                self.discriminator.trainable_weights
            )
        )

        del g_tape
        del d_tape

        return {
            "g_loss": g_loss,
            "d_loss": d_loss,
        }


def create():
    generator, discriminator = createDG()
    return generator, discriminator
