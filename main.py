import argparse
import trainModel
import predict
import sys


def noInput():
    print("no input video", file=sys.stderr)
    exit(1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--video", type=str, help="path to video for upscaling")
    parser.add_argument("--job", type=str, help="choose to \"train\" or \"predict\"")
    #parser.add_argument("--save", action="store_true", help="save model")
    args = parser.parse_args()

    if args.job is None:
        print("no job specified", file=sys.stderr)
        return 1
    else:
        if args.job == "train":
            print("training model")
            trainModel.train()
        elif args.job == "predict":
            print("predicting from video")
            predict.predict(args.video)
    #if args.save:
    #    predict.saveModel()


if __name__ == '__main__':
    main()
