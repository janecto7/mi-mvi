from constants import *
import createModel
import glob
import numpy as np
from PIL import Image
import tensorflow as tf
from createModel import Gan
import pandas as pd
from keras.utils.vis_utils import plot_model

def getData():
    X = []
    Y = []
    for video in VIDEO_NAMES:
        pathStart = 'test_path' + '/' + video

        frames = sorted(glob.glob(pathStart + '*'))
        for i, frame in enumerate(frames):
            if i == 0 or i == len(frames) - 1:
                continue
            else:
                X.append(
                    np.concatenate([np.asarray(Image.open(frames[i - 1])),
                                    np.asarray(Image.open(frames[i + 1]))],
                                   -1)
                )
                Y.append(np.asarray(Image.open(frame)))
    print(np.array(X) / np.array((255.0)))
    print(np.array(Y) / np.array(255.0))
    return np.array(X) / np.array(255.0), np.array(Y) / np.array(255.0)


def showProgress():
    pass


class DataGenerator(tf.keras.utils.Sequence):

    def __get_image(self, path):
        image = tf.keras.preprocessing.image.load_img(path)
        image_arr = tf.keras.preprocessing.image.img_to_array(image).numpy()
        return image_arr / 255.

    def get(self, index):
        X = []
        Y = []
        cnt = 0
        # print(" delka pole: " + str(len(self.frameNames)) + " index: " + str(self.i))
        while self.i < len(self.frameNames):
            if self.i + 1 >= len(self.frameNames) - 20:
                self.i = 1
                return self.last
            if self.frameNames[self.i - 1][0] != self.frameNames[self.i][0] or self.frameNames[self.i][0] != \
                    self.frameNames[self.i + 1][0]:
                continue

            if cnt == BATCH_SIZE:
                self.last = X, Y
                return np.array(X) / np.array(255.0), np.array(Y) / np.array(255.0)
            X.append(
                np.concatenate([np.asarray(Image.open(self.frameNames[self.i - 1])),
                                np.asarray(Image.open(self.frameNames[self.i + 1]))],
                               -1)
            )
            Y.append(np.asarray(Image.open(self.frameNames[self.i])))
            cnt += 1
            self.i += 1

    def __len__(self):
        return (len(self.frameNames) - 20) // BATCH_SIZE

    def __getitem__(self, index):
        X, Y = self.get(index)
        return X, Y

    def __init__(self):
        self.i = 1
        self.frameNames = sorted(glob.glob(DATA_DIR_RESIZED + '/*'))
        self.last = None


def train():
    generator, discriminator = createModel.create()

    gan = Gan(
        discriminator=discriminator, generator=generator
    )
    plot_model(gan.discriminator, to_file='model_plot_disc.png', show_shapes=True, show_layer_names=True)

    plot_model(gan.generator, to_file='model_plot_gen.png', show_shapes=True, show_layer_names=True)
    gan.compile(
        d_optimizer=tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE),
        g_optimizer=tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE),
    )
    print("training started")
    history = gan.fit(DataGenerator(), epochs=EPOCHS_NUM)
    hist_df = pd.DataFrame(history.history)
    hist_csv_file = 'history.csv'
    with open(hist_csv_file, mode='w') as f:
        hist_df.to_csv(f)

    gan.generator.save(GENERATOR_LATEST_WEIGHTS)
    gan.discriminator.save(DISCRIMINATOR_LATEST_WEIGHTS)
