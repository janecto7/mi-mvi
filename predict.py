from createModel import create, Gan
from constants import *
from keras.models import load_model
import glob
import copy
import numpy as np
from PIL import Image
import cv2


def framesToArray(filename1, filename2):
    arr = np.expand_dims(np.concatenate([np.asarray(Image.open(filename1)) / 255.0,
                                          np.asarray(Image.open(filename2)) / 255.0], -1), axis=0)
    return arr

def savePrediction(upscaled):
    for i, frame in enumerate(upscaled):
        frame = ((frame.squeeze(0)) * 255).astype(np.uint8)
        Image.fromarray(frame).save(OUTPUT_DIR + '/us' + str(i) + '.jpg')


def predict(predictVideoDir):
    generator = load_model(GENERATOR_LATEST_WEIGHTS)
    generator.compile()

    pathStart = predictVideoDir + '/' #PREDICT_DIR_RESIZED + '/' + PREDICT_VIDEO.split('.')[0]
    frames = sorted(glob.glob(pathStart + '*'))
    upscaled = []
    i = 0
    while i < len(frames) - 1:
        predicted = generator.predict(framesToArray(frames[i], frames[i + 1]))
        upscaled.append(predicted)
        i += 1
    savePrediction(upscaled)
