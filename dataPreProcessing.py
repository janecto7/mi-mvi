import PIL.Image
import cv2
import os

from constants import *
from PIL import Image


def createData(videos):
    for video in videos:
        f = cv2.VideoCapture(VIDEOS_DIR + '/' + video)
        success, image = f.read()
        cnt = 0
        os.mkdir(DATA_DIR)
        while success:
            cv2.imwrite(DATA_DIR + '/' + noExName + str(cnt) + '.jpg', image)
            success, image = f.read()
            print('Read a new frame: ', success)
            cnt += 1


def resizeImages(inDir, outDir):
    fileList = os.listdir(inDir)
    print(fileList)
    for file in fileList:
        im = Image.open(inDir + '/' + file)
        im.thumbnail((320, 180), PIL.Image.ANTIALIAS)
        im.save(outDir + '/' + file)
        print("resized " + file)


def createPrediction():
    f = cv2.VideoCapture(PREDICT_VIDEO_DIR + '/' + PREDICT_VIDEO)
    success, image = f.read()
    cnt = 0
    noExName = PREDICT_VIDEO.split('.')[0]
    while success:
        cv2.imwrite(PREDICT_DIR + '/' + noExName + str(cnt) + '.jpg', image)
        success, image = f.read()
        print('Read a new frame: ', success)
        cnt += 1


if __name__ == '__main__':
    # createData(os.listdir(VIDEOS_DIR))
    # resizeImages(DATA_DIR, DATA_DIR_RESIZED)
    #createPrediction()
    resizeImages(PREDICT_DIR,PREDICT_DIR_RESIZED)