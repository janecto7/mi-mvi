# Podrobné zadání SP
    
Video Frame Rate Upscaling Using Neural Networks

# Pokyny k dotažení potřebných velkých dat, jsou-li mimo GIT repo
- stáhnutí videí z youtube například pomocí youtube-dl
- rozdělení videa na snímky pomocí dataPreprocessing.py

    
# Pokyny ke spuštění
## Pro predikci
`main.py --video <path-to-frames> --job predict`
## Pro trénování
`main.py --job train`